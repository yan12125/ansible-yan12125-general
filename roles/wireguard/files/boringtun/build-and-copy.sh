buildah bud -t boringtun-builder --layers=true
# Copying files does not require starting the container!
# See: https://stackoverflow.com/a/51186557
podman create --name boringtun-builder boringtun-builder
podman cp boringtun-builder:/root/.cargo/bin/boringtun .
podman rm boringtun-builder
