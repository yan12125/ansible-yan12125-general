import netaddr

# inspired by https://www.procustodibus.com/blog/2021/03/wireguard-allowedips-calculator/

def routes_exclude(value):
    # https://netaddr.readthedocs.io/en/latest/tutorial_03.html
    routes, routes_to_exclude = value

    ret = netaddr.IPSet()
    for route in routes:
        ret |= netaddr.IPSet(netaddr.IPNetwork(route))
    for route_to_exclude in routes_to_exclude:
        ret -= netaddr.IPSet(netaddr.IPNetwork(route_to_exclude))

    ret_list = []
    for cidr in ret.iter_cidrs():
        ret_list.append(str(cidr))

    return ret_list

class FilterModule(object):
    def filters(self):
        return {
            'routes_exclude': routes_exclude
        }
