import json
import subprocess

from ansible.plugins.vars import BaseVarsPlugin
from ansible.utils.display import Display

display = Display()

_terraform_cache = {}

class VarsModule(BaseVarsPlugin):
    def get_vars(self, loader, path, entities, cache=True):
        super().get_vars(loader, path, entities)

        if not _terraform_cache:
            try:
                terraform_output = subprocess.check_output(["terraform", "output", "-json"])
            except FileNotFoundError:
                display.warning('Terraform not fonud. Skipping loading variables.')
            else:
                _terraform_cache.update(
                    json.loads(terraform_output)
                )

        return {
            'terraform_output': {
                name: details['value'] for name, details in _terraform_cache.items()
            }
        }
